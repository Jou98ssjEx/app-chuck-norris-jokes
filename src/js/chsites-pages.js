import { getChiste } from "./http-provider";

const body = document.body;
let btnAgg, olList;

const createChisteHtml = () => {
    const html = `
        <h1 class=" text-center mt-4">App "chuck-norris" Jokes</h1>
        <hr class="mt-4">

        <button class="btn btn-outline-primary"> Get Joke</button>

        <ol class="mt-3 list-group">
         
        </ol>
    `;

    const divChistes = document.createElement('div');

    divChistes.innerHTML = html;

    body.append(divChistes);
}


// Create li chiste
// { id, value, icon_url }
const dawChiste = (chiste, count) => {
    const olItem = document.createElement('li');

    olItem.innerHTML = `<b class="mb-3"> ${count}. Id: </b>${ chiste.id} <br> <b>Joke:</b> ${chiste.value}`;
    olItem.classList.add('list-group-item');

    let imgChiste = document.createElement('img');
    // console.log(imgChiste);
    imgChiste.classList.add('rounded');
    imgChiste.classList.add('float-end');
    imgChiste.src = chiste.icon_url;
    imgChiste.alt = 'img-api';

    olItem.append(imgChiste);

    olList.append(olItem);
}

const events = () => {
    btnAgg = document.querySelector('button');
    olList = document.querySelector('ol');

    try {
        let c = 0;

        btnAgg.addEventListener('click', async() => {
            console.log('Nuevo Chiste');
            // c = c + 1;
            c++;
            btnAgg.disable = true;

            dawChiste(await getChiste(), c);
            btnAgg.disable = false;
            // try {

            // } catch (error) {
            //     throw error
            // }
            // const data = getChiste().then();
            // console.log({ data });
            // dawChiste(await getChiste(), c);
        });
    } catch (error) {
        throw error
    }


}




export const init = () => {
    createChisteHtml();
    events();
}