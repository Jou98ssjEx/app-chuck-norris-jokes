const jokeUrl = 'https://api.chucknorris.io/jokes/random';
// const userData = 'https://reqres.in/api/users?page=2';

// // CLoudinari
// const cloudPreset = 'ml_default';
// const cloudUrl = 'https://api.cloudinary.com/v1_1/dgekgbspr/upload';

const getChiste = async() => {

    try {
        const resp = await fetch(jokeUrl);

        if (!resp.ok) throw 'Error en la peticion';

        const { icon_url, id, value } = await resp.json();

        return { icon_url, id, value }

    } catch (error) {
        throw error
    }

}

// const getUsers = async() => {
//     try {
//         const resp = await fetch(userData);
//         if (!resp.ok) throw 'Error en la peticion';

//         const { data: users } = await resp.json();
//         // console.log(users);

//         return users

//     } catch (error) {
//         throw error
//     }
// }


// const uploadFile = async(file) => {

//     const formData = new FormData();
//     formData.append('upload_preset', cloudPreset);
//     formData.append('file', file);

//     try {
//         const resp = await fetch(cloudUrl, {
//             method: 'POST',
//             body: formData
//         });

//         if (resp.ok) {
//             const data = await resp.json();
//             // console.log(data);
//             return data.secure_url;
//         } else {
//             throw await resp.json();
//         }
//     } catch (error) {
//         throw error
//     }
// }


export {
    getChiste
    // getUsers,
    // uploadFile
}